# Sigma-Rules

A list of helpful detections written in the Sigma specification for ease of importing it into SIEM-specific languages. This repo is mainly for my own learning and referencing, however if you find this useful please feel free to use these rules.

_Note: This is a work in progress and will be actively worked on come the first half of 2024._